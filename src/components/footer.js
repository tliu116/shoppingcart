import React, {Component} from 'react'


export class Footer extends Component {
    render() {
        return (
        <div className="footer">
            <div className="footerText">
                <div className="footerColumn">
                    <p><strong>Customer Service</strong></p>
                    <p>Contact Us</p>
                    <p>FAQ</p>
                    <p>Returns and Exchanges</p>
                    <p>Shipping and Delivery</p>
                    <p>Warranty and Service</p>
                    <p>Site Feedback</p>
                    <p>Track Your order</p>
                </div>
                <div className="footerColumn">
                    <p><strong>News & Resources</strong></p>
                    <p>For Business</p>
                    <br/>
                    <p><strong>Locations</strong></p>
                    <p>Find a Retailer</p>
                    <p>Our New York Store</p>
                </div>
                <div className="footerColumn">
                    <p><strong>About HmM</strong></p>
                    <p>About Us</p>
                    <p>HmM.com</p>
                    <p>Our Designers</p>
                    <p>Request a Catalog</p>
                    <p>Careers</p>
                    <p>Site Feedback</p>
                </div>
                <div className="footerColumn">
                    <p><strong>Join Our Mailing List</strong></p>
                    <p><strong>Follow Us</strong></p>
                </div>
            </div>

        </div>
        )
    }
}