import React, {Component} from 'react';
import {connect} from 'react-redux';
import {actFetchItem, actSelectItem} from "../actions";

class ItemDetail extends Component {

    componentDidMount() {
        if (this.props.items.length < 1) {
            this.props.actFetchItem()
        } else {
            console.log('data fetched')
        }
    }

    renderList() {
        return this.props.items?.map((product, index) => {
          const picSource = product.media.split("|")[0]
          let quantity = 0
            this.props.cartItem?.map((item) => {
                if (item.id === product.id) {
                    quantity = item.quantity
                }
            })
            return (
                <div key={index} className="item">
                    <img src={picSource} alt="pic"/>
                    <div className="title">{product.name}</div>
                    <div className="price">C$&nbsp;{product.price}</div>
                    <div className="color">
                        <span className="square white"> </span>
                        <span className="square red"> </span>
                        <span className="square darkGreen"> </span>
                        <span className="square gray"> </span>
                        <span className="square darkGray"> </span>
                        <span className="square darkBlue"> </span>
                    </div>
                    <div className="shipping">Free Shipping On Everything</div>
                    <button className="btn" onClick={() => {
                        this.props.actSelectItem(product, 1, parseInt(product.price))
                    }}>Add To Cart</button>
                    <span style={{marginLeft:"10px", fontSize:"14px"}}>Quantity: {quantity}</span>
                </div>

            )
        })
    }

    render() {
        return (
            <div className="container">
                <h1>Office Chairs</h1>
                <div className="sort">
                    <div>
                        <span>Price</span>
                        <span>Material</span>
                        <span style={{marginRight: "5px"}}>Sort By: </span>
                        <form className="sortForm">
                            <select name="sort" id="sort">
                                <option value="Featured Products">Featured Products</option>
                                <option value="Popularity">Popularity</option>
                                <option value="Rating">Rating</option>
                                <option value="Price">Price</option>
                            </select>
                        </form>
                    </div>
                    <div>Showing 10 of 10 items</div>
                </div>
                <div className="list">
                    {this.renderList()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log('state', state)
    return {
        items: state.fetchItemReducer,
        cartItem: state.cartReducer.cartItem
    }
}

const mapDispatchToProps = {
    actFetchItem, actSelectItem
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail)