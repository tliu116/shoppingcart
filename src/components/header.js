import React, {Component} from 'react';
import logo from './hm-logo-caption.svg';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";


class Header extends Component {

    render() {
        const nav = ["New", "Office", "Living", "Dining", "Bedroom", "Outdoor", "Lighting", "Accessories"]
        let quantity = 0
        this.props.cartItem?.map((item) => {
            quantity += parseInt(item.quantity)
        })
        return (
            <div className="container">
                    <div className="aboveHeader">
                        <div>
                            <span>Store</span>
                            <span>Contact</span>
                        </div>
                        <div><span>Customer Service &nbsp;&nbsp; 888 443 4357</span></div>
                        <div>
                            <span>My Account</span>
                            <Link to="/cart" type="button" className="link">Cart&nbsp;<i className="fas fa-shopping-cart"></i>&nbsp; {quantity}</Link>
                            <span>Order History</span>
                        </div>
                    </div>
                <hr />
                <div className="header">
                    <div className="nav">
                        <Link to="/" type="button"><img src={logo} width="180px" alt="logo" /></Link>
                        <div className="navItems">
                            {
                                nav.map((value, index) => {
                                    return (
                                        <span key={index}>{value}</span>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <form className="search">
                        <input type="text" placeholder="Search"/>
                    </form>
                </div>
                <hr />
                <div className="breadCrumb">
                    <span>Home</span>
                    <span> > </span>
                    <span>Office</span>
                    <span> > </span>
                    <span>Office Chairs</span>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.fetchItemReducer,
        cartItem: state.cartReducer.cartItem
    }
}


export default connect(mapStateToProps, {})(Header)