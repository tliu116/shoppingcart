import React, {Component} from 'react';
import {connect} from 'react-redux';
import {actUpdateQuantity, actRemoveItem} from "../actions";


class Cart extends Component {

    state = {
        subTotal: 0
    }

    componentDidMount() {
        this.updateSubTotal()
    }

    updateSubTotal() {
        let subTotal = 0
        this.props.cartItem?.map((item) => {
            subTotal += parseInt(item.totalPrice)
        })
        this.setState({subTotal: subTotal})
    }


    renderCart() {
        return this.props.cartItem?.map((item, index) => {
            const picSource = item.media.split("|")[0]
            const quantity = item.quantity
            const total = item.totalPrice
            let {subTotal} = this.state
            return (
                <tbody key={index} className="product">
                    <tr>
                    <td className="productName">
                        <img src={picSource} alt="pic"/>
                        <div>{item.name}</div>
                    </td>
                    <td className="unitPrice">C$&nbsp;{item.price}</td>
                    <td className="quantity">
                        <form>
                        <input type="number" value={quantity}
                                onChange={(e) => {
                                    if (parseInt(e.target.value) < 1) {e.target.value = 1}
                                    this.props.actUpdateQuantity(item, parseInt(e.target.value), parseInt(e.target.value) * item.price)
                                    subTotal += parseInt(e.target.value) * item.price - quantity * item.price
                                    this.setState({subTotal: subTotal})
                                }} />
                        </form>
                    </td>
                    <td className="total">
                        <button className="btnSmall" onClick={() => {
                            subTotal -= quantity * item.price
                            this.setState({subTotal: subTotal})
                            this.props.actRemoveItem(item)

                        }}>Remove</button>
                        <span>C$&nbsp;{total}</span>
                    </td>
                    </tr>
                </tbody>
            )

        })
    }

    render() {
        let {subTotal} = this.state
        const tax = parseInt(0.07 * subTotal)
        const delivery = parseInt(0.04 * subTotal)
        return(
            <div className="container">
                <h1 style={{textAlign: "left"}}>My Cart</h1>
                <table id="t1">
                    <thead>
                    <tr className="tableHeader">
                        <td>Product Information</td>
                        <td>Price</td>
                        <td>Quantity</td>
                        <td>Total</td>
                    </tr>
                    </thead>
                    {this.renderCart()}
                </table>
            <div className="summary">
                <div style={{width:"65%", marginRight:"20px"}}>
                    <h3>100% Satisfaction Guarantee</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
                <div style={{width:"35%"}}>
                    <table id="t2">
                        <tbody>
                        <tr>
                            <td className="summaryTitle">Subtotal</td>
                            <td>C$&nbsp;{subTotal}</td>
                        </tr>
                        <tr>
                            <td className="summaryTitle">Estimated Tax</td>
                            <td>C$&nbsp;{tax}</td>
                        </tr>
                        <tr>
                            <td className="summaryTitle">Delivery: FedEx</td>
                            <td>C$&nbsp;{delivery}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

        )
    }

}

const mapStateToProps = (state) => {
    console.log('state', state.cartReducer)
    return {
        cartItem: state.cartReducer.cartItem
    }
}

export default connect(mapStateToProps, {actUpdateQuantity, actRemoveItem})(Cart)