import {combineReducers} from 'redux';
import {FETCH_ITEM, SELECT_ITEM, REMOVE_ITEM, UPDATE_QUANTITY} from "../helper";

const initialState = {
    cartItem: []
}

const fetchItemReducer = (state=[], action) => {
    if (action.type === FETCH_ITEM) {
        return [...state, ...action.payload.data]
    }
    return state
}

const cartReducer = (state = initialState, action) => {
    if (action.type === SELECT_ITEM) {
        if (state.cartItem.find((item) => item.id === action.payload.id)) {
            console.log('enter find')
            let tempCart = state.cartItem.map(item => item.id === action.payload.id ?
                {...item, quantity:parseInt(item.quantity) + parseInt(action.payload.quantity),
                totalPrice: (parseInt(item.quantity) + parseInt(action.payload.quantity)) * item.price} :
                {...item})
                return {...state, cartItem: tempCart}
            } else {
            console.log('action payload', action.payload)
            return {...state, cartItem: [...state.cartItem, action.payload]}
        }
    }

    if (action.type === UPDATE_QUANTITY) {
        if (parseInt(action.payload.quantity) === 0) {
            let tempQuantity = state.cartItem.filter(item => item.id !== action.payload.id)
            return {...state, cartItem:tempQuantity}
        } else {
            let tempQuantity = state.cartItem.map(item => item.id === action.payload.id ? {
                ...item, quantity:parseInt(action.payload.quantity), totalPrice: action.payload.totalPrice} : {...item})
            return {...state, cartItem:tempQuantity}
        }
    }

    if (action.type === REMOVE_ITEM) {
        let tempCart = state.cartItem.filter(item => item.id !== action.payload.id);
        return {...state, cartItem: tempCart}
    } else {
        return state
    }


}

export default combineReducers({
    fetchItemReducer,
    cartReducer
})