import {FETCH_ITEM, SELECT_ITEM, REMOVE_ITEM, UPDATE_QUANTITY} from "../helper";
import jsonPlaceHolder from "../api/jsonPlaceHolder";

export const actFetchItem = () => {
    return async dispatch => {
        let items = await jsonPlaceHolder.get('product')
        dispatch({
            type: FETCH_ITEM,
            payload: items.data
        })
    }
}

export const actSelectItem = (item, quantity, totalPrice) => {
    return {
        type: SELECT_ITEM,
        payload: {...item, quantity, totalPrice}
    }
}

export const actUpdateQuantity = (item,quantity, totalPrice) => {
    return {
        type: UPDATE_QUANTITY,
        payload: {...item, quantity, totalPrice}
    }
}

export const actRemoveItem = (item) => {
    return {
        type: REMOVE_ITEM,
        payload: item
    }
}