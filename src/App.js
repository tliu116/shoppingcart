import React, {Component}from 'react';
import './shoppingCart.css';
import {BrowserRouter, Route} from 'react-router-dom';
import ItemDetail from "./components/itemDetail";
import {Footer} from "./components/footer";
import Cart from "./components/cart"
import Header from "./components/header";


class HomePage extends Component {


    render() {
        return(
            <div>
                <ItemDetail />
            </div>
        )
    }
}

class CartPage extends Component {



    render() {
        return(
            <div>
                <Cart />
            </div>
        )
    }
}



class App extends Component {

  render() {
      return (
      <div>
          <BrowserRouter>
              <div className="notice">Enjoy Free Shopping + 0% Financing Available</div>
              <Header />
              <div>
                  <Route path="/" exact component={HomePage}></Route>
                  <Route path="/cart" exact component={CartPage}></Route>
              </div>
              <Footer />
          </BrowserRouter>
      </div>
  )
  }

}

export default App;

